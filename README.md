Overview
--------

The VideoJS Mediablock module is an ADA/508 compliant, Responsive media player that works well with the Media and Media Library modules in Drupal core. [VideoJS](https://videojs.com/) is a very mature, open-source video library primarily sponsored by Brightcove, with [a wealth of plugins](https://videojs.com/plugins/).

Without the use of a mediaplayer as this module provides, by default, visitors to your website will experience audio/video media displayed using whatever internet client-browser is in-use, and under its own negotiated terms. This module provides Drupal site builders and developers complete control while delivering ease-of-use to content managers.

Features
--------

*   Compliant with the Americans with Disabilities Act (ADA) standards for video, by providing configurable, themable, and multilingual closed-captions support.
    
*   The VideoJS Mediablock module creates a custom blocktype with three fields to input a Media file, an optional poster image, and an optional subtitle file.
    
*   Responsive, with no need to input height or width values. Simply input a media source -- possibly YouTube.
    
*   **New**, Support for [SDC](https://www.drupal.org/docs/develop/theming-drupal/using-single-directory-components) custom theming.
    
*   **New**, Remote hosting is supported as well as local server hosting.
    
*   **New**, MP3 audio files are supported as well as MP4 videos.
    
*   **New**, Videos hosted on YouTube are supported.
    
*   Besides the default player theme, [four more are available](https://www.mux.com/blog/the-new-video-js-themes). And of course it is easy to [customize media player themes](https://videojs.com/guides/skins/).  
      
    For example, [https://www.thrashermagazine.com](https://www.thrashermagazine.com "Thrasher magazine") uses a custom media player theme for their older videos. Like [this one](https://www.thrashermagazine.com/articles/videos/skateline-06-11-2024/). Please note, _https://www.thrashermagazine.com is using [a completely different, commercial media player](https://jwplayer.com/)_, not this free, open-source Drupal VideoJS Mediablock module.
    
*   Developed around the core Media and Media Library modules.
    

Required modules
----------------

Drupal **core** modules: Media Library, Config, Field, Block Content, SDC, Serialization

Drupal **contrib** module: [File Upload Secure Validator](https://www.drupal.org/project/file_upload_secure_validator)

Other Requirements
------------------

NPM must be installed on the server.  
The php library `fileinfo` must be installed on the server.

Drupal 10.3 or higher is required, [and this patch](https://git.drupalcode.org/project/drupal/-/merge_requests/3094.diff) is required for Drupal versions 10.0 - 10.2. More information about the patched issue is here:

*   [Issue 3062376: Template suggestions for custom block view modes](https://www.drupal.org/project/drupal/issues/3062376#comment-14885212)

Instructions
------------

**Differences between Drupal 10 & Drupal 11**  

Drupal 10 must be patched or this module will not function.

Drupal 10 requires the SDC (experimental) module be installed. There is no SDC module in Drupal 11 because that code has been implemented directly into core Drupal code. Because the 2.1.x branch has been developed for ^Drupal 11 the SDC requirement has been removed, _and must be manually enabled on Drupal 10 websites_.

**Installation**  
Upon installation, from within the module's root folder, you must install NPM related libraries with the '`npm install`', '`npm clean install`', or '`npm update`' commands. Over time, you'll need to keep these libraries up-to-date using NPM.

Navigate to `/admin/config/media/file_upload_secure_validator`. Copy the following lines to ensure filetype upload security, adding them to the bottom of the list:

    video/quicktime,video/mp4,video/x-m4v,video/ogg,video/webm,video/x-flv,video/x-f4v

    audio/x-aac,audio/x-flac,audio/x-mpeg,audio/mpeg,audio/mp4,audio/ogg,application/x-ogg,audio/vorbis,audio/x-wav

    text/vtt,text/plain

**Usage**  
Create a new custom VideoJS Mediablock at `/block/add/videojs_mediablock`. The media field is required, while the poster and subtitle fields are optional.

New VideoJS Mediablocks will be listed at `/admin/structure/block/block-content`.

Place your custom blocks anywhere at `/admin/structure/block` or use Layout Builder.

Roadmap
-------

*   Version 1.0.x is deprecated.
    
*   Version 2.0.x is still a stable version although now that 2.1.x has been released at some point it will become deprecated. It was refactored to include a better uninstaller, SDC support, and a development path forward.
    
*   Version 2.1.x is the current stable branch. Code to support audio and remote media file hosting, including YouTube is new. The user-interface to support these features has also been developed along with the requisite field validation. [Click here to see an animation.](https://www.drupal.org/files/project-images/videojs_mediablock_edit1.gif)
    

Version 2.2.x is the current development branch. New features will be developed here.

You can [buy me a coffee ☕](https://www.buymeacoffee.com/amstercad) if you're so inclined -- I'll be grateful.
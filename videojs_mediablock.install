<?php

/**
 * @file
 * Install, update and uninstall functions for the VideoJS Media Block module.
 */

/**
 * Implements hook_requirements().
 */
function videojs_mediablock_requirements($phase) {
  $requirements = [];

  if ($phase == 'install') {
    // Check if required modules exist
    if (!Drupal::moduleHandler()->moduleExists('media')) {
      $requirements['videojs_mediablock_media'] = [
        'title' => t('Media'),
        'description' => t('VideoJS Media Block requires the Media module to be installed.'),
        'severity' => REQUIREMENT_ERROR,
      ];
    }
  }

  return $requirements;
}

/**
 * Implements hook_install().
 */
function videojs_mediablock_install() {
  // Create required media types if they don't exist
  _videojs_mediablock_create_media_types();
}

/**
 * Helper function to create required media types.
 */
function _videojs_mediablock_create_media_types() {
  $media_types = [
    'audio' => [
      'label' => 'Audio',
      'description' => 'Audio files for VideoJS player.',
      'source' => 'audio_file',
    ],
    'video' => [
      'label' => 'Video',
      'description' => 'Video files for VideoJS player.',
      'source' => 'video_file',
    ],
  ];

  $media_storage = \Drupal::entityTypeManager()->getStorage('media_type');

  foreach ($media_types as $id => $type) {
    // Only create if it doesn't exist
    if (!$media_storage->load($id)) {
      $media_storage->create([
        'id' => $id,
        'label' => $type['label'],
        'description' => $type['description'],
        'source' => $type['source'],
        'source_configuration' => [
          'source_field' => 'field_media_' . $type['source'],
        ],
      ])->save();

      // Log the creation
      \Drupal::logger('videojs_mediablock')->notice(
        'Created @type media type.',
        ['@type' => $type['label']]
      );
    }
  }
}
